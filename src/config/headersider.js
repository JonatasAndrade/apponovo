import React, { Component } from 'react';
import { Icon } from 'native-base';
import { Image, View } from "react-native";
import { withNavigation } from "react-navigation";
import { DrawerActions } from "react-navigation-drawer";

const HeaderSider = ({ navigation }) => (
  <View style={{height: 50,justifyContent:'space-between',flexDirection:'row',backgroundColor: "#0090ff"}}>

    <Icon name='arrow-left' type={'FontAwesome'} 
    onPress={() => navigation.navigate('Noticias')} 
    style={{ fontSize:32, color:'white', resizeMode: 'contain',width: 32, height: 32, margin:8 }} />

    <Image
      source={require("../../assets/images/logo-onovo-branco.png")} style={{  resizeMode: 'contain',width: 100, height: 42, margin:8}}
    />

    <Icon onPress={() => navigation.navigate('Pesquisa')} name='search' type={'FontAwesome'} style={{ fontSize:25, color:'white', resizeMode: 'contain',width: 40, height: 42, marginTop:13 }} />

    </View>
);

export default withNavigation(HeaderSider);




