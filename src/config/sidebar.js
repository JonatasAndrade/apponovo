import { StatusBar } from "react-native";

StatusBar.setBackgroundColor("#0090ff");
StatusBar.setBarStyle("light-content");