import React, { Component } from 'react';
import "./config/reactotron";
import "./config/sidebar";
import { createStackNavigator, createAppContainer } from "react-navigation";

import Inicio from "./screens/inicio";
import Noticias from "./screens/noticias";
import NoticiaLer from "./screens/noticiaLer";
import DrawerCat from "./screens/drawer";
import CategoriaShow from "./screens/categoriasShow";
import Login from "./screens/login";


import Pesquisa from "./screens/pesquisa";
import Drawer from "./screens/drawer/Drawer";

const Navegador = createStackNavigator({

	Inicio: {
		screen: Inicio,
		navigationOptions: () => ({
			header: null,
			tabBarVisible: false
		})
	},
	Login: {
		screen: Login,
		navigationOptions: () => ({
			header: null,
			tabBarVisible: false
		})
	},
	Drawer,
    Pesquisa,
	
		Noticias,
		 NoticiaLer: {
			screen: NoticiaLer,
		},
		
		DrawerCat,
		 CategoriaShow: {
			screen: CategoriaShow,
		},



}, {
	headerMode: 'none'
});

export default createAppContainer(Navegador);