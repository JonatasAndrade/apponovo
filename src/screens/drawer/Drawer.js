import React, { Component } from "react";
import { SafeAreaView, View, StyleSheet, ScrollView } from "react-native";
import { createDrawerNavigator, DrawerItems } from "react-navigation";

//DrawerContent

import Header from "./components/header";
import Categorias from "./index";
import Footer from "./components/footer";

//screen
import Home from "../home";
import Tabs from "../tabs";

const DrawerContent = props => (
  <SafeAreaView style={styles.container}>
    <View style={{ flex: 1, justifyContent:'space-between' }}>
     
      <View style={styles.header}>
        <Header />
      </View>

      <ScrollView>
      <View style={{ marginTop: 100}}>

        <Categorias />
      </View>
      </ScrollView>

       <View>
        <Footer />
      </View>

    </View>
  </SafeAreaView>
);

const DrawerNav = createDrawerNavigator(
  {
    Tabs,
    Home: {
      screen: Home
    }
  },
  {
    contentComponent: DrawerContent,
    drawerBackgroundColor: "#FFFFFF",
    contentOptions: {
      activeTintColor: "#FFF",
      labelStyle: {
        fontSize: 20,
        color: "#646464"
      }
    }
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    height: 70,
    backgroundColor:'#0090ff',
    alignItems: "center",
    justifyContent: "center",
  }
});

export default DrawerNav;
