import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
 container: {
    flex:1,
  },
  tbotao:{
  	fontSize:20,
  	color:'#808080',
    marginLeft:10,
    marginBottom:10,
    fontWeight:'100'
  }
});

export default styles;