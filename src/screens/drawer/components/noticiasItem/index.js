import React from "react";
import { TouchableHighlight, Text, View } from "react-native";
import styles from "./style";
import propTypes from "prop-types";

const NoticiaItem = ({ data, navigation }) => (

  
  <View style={styles.container} >
     
        <TouchableHighlight
      onPress={() => navigation.navigate("CategoriaShow", { category: data.id })}  underlayColor="#FFFFFF" >

      <Text style={styles.tbotao}>{data.title}</Text>

        </TouchableHighlight>

  </View>
);

NoticiaItem.propTypes = {
  data: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.string,
      title: propTypes.string
    })
  ).isRequired,
  navigation: propTypes.object.isRequired
};

export default NoticiaItem;
