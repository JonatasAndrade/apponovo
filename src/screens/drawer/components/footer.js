import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { Icon } from 'native-base';
import { withNavigation } from "react-navigation";
import { DrawerActions } from "react-navigation-drawer";

const Footer = ({ navigation }) => (
  
      <View style={styles.container}>

      <Icon name='arrow-left' type={'FontAwesome5'}onPress={() => navigation.navigate('Home')} style={{fontSize: 32, color: 'white', padding:10}} />

      <Icon name='close' type={'FontAwesome'}onPress={() => navigation.navigate('Login')} style={{fontSize: 32, color: 'white', padding:10}} />
      </View>
  
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#0090ff",
    height: 50,
    justifyContent:'space-between',
    flexDirection:'row'
  }
});

export default withNavigation(Footer);
