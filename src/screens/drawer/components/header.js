import React, { Component } from "react";
import { Text, StyleSheet, View, Image } from "react-native";

export default class Header extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={{ uri:'http://www.auctus.com.br/wp-content/uploads/2017/09/sem-imagem-avatar.png'}}
          style={{
            width: 100,
            height: 100,
            borderRadius: 50,
            borderWidth: 5,
            borderColor: "#FFF"
          }}
        />
        <Text style={{ fontSize: 20, marginTop: 5, color: "#646464" }}>
          Convidado
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 100,
  }
});
