import React, { Component } from "react";
import { View, ScrollView, FlatList, Text, Image, ActivityIndicator } from "react-native";
import api from "../services/api";
import styles from "./style";
import NoticiaCat from "./components/noticiasItem";
import { withNavigation } from "react-navigation";

class Noticias extends Component {
  state = {
    categorias: [],
    loading: true
  };

  componentDidMount() {
    this.loadCategories();
    this.setState({ loading: false });
  }

  loadCategories = async () => {
    try {
      const {
        data: { categorias }
      } = await api.get("/");
      this.setState({ categorias });
    } catch (err) {}
  };

  render() {
    const { loading } = this.state;

    if (loading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
    
      <View >
        

        <ScrollView pagingEnable={true}>
          
          <FlatList
            data={this.state.categorias}
            renderItem={({ item }) => (
              <NoticiaCat navigation={this.props.navigation} data={item} />
            )}
            keyExtractor={item => item.id}
          />

        </ScrollView>
      </View>
    );
  }
}

export default withNavigation(Noticias);