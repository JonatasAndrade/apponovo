import React, { Component } from "react";
import { View, Image, Text, ScrollView, FlatList } from "react-native";
import Header from "../../config/header";
import styles from "./style";
import BannerH1 from "./components/bannerh1";
import api from "../services/api";

export default class Home extends Component {
  state = {
    data: []
  };

  componentDidMount() {
    this.loadCategories();
  }

  loadCategories = async () => {
    try {
      const {
        data: { data }
      } = await api.get("/banners/list?posicao=mobilehome1");
      this.setState({ data });
    } catch (err) {}
  };

  render() {
    return (
      <View style={styles.body}>
       <ScrollView pagingEnable={true}>
          <Header />
        <FlatList
            data={this.state.data}
            renderItem={({ item }) => (
              <BannerH1 navigation={this.props.navigation} data={item} />
            )}
            keyExtractor={item => item.id}
          />

        </ScrollView>
      </View>
    );
  }
}
