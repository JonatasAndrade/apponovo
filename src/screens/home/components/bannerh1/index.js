import React from "react";
import propTypes from "prop-types";
import { Image, Text, View } from "react-native";
import styles from "./style";

const NoticiaItem = ({ data, navigation }) => (

  
  <View style={styles.container} >
     
      <View style={{ flex: 1,marginTop:-40,flexDirection: 'column',justifyContent: 'center',alignItems: 'center', }}>
      <Image style={{ width:400, height:200, marginBottom:-20,resizeMode:'contain'}} source={{uri: data.image.source}} />
      </View>

  </View>
);

NoticiaItem.propTypes = {
  data: propTypes.arrayOf(
    propTypes.shape({
      image: propTypes.string,
      source: propTypes.string,
    })
  ).isRequired,
  navigation: propTypes.object.isRequired
};

export default NoticiaItem;
