import axios from 'axios'

const api = axios.create({
	
  baseURL: 'http://app.grupoevve.com.br/api',
  headers: {'api-token': '215C63060F117',
  			'api-uri': 'o-novo',
	},

})

export default api;