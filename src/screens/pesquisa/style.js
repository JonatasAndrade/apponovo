import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  viewStyle: {
    justifyContent: 'center',
    flex: 1,
    backgroundColor:'white',
  },
  textStyle: {
    padding: 10,
  },
});

export default styles;