import React from "react";
import propTypes from "prop-types";
import { View, Text, Image } from "react-native";

import styles from "./style";

const Noticia = ({ data }) => (

  <View style={styles.container} >

      <Text style={styles.title}>{data.title}</Text>

      <Text style={styles.legenda}>{data.legendaPrincipal}</Text>

      <Text style={styles.datehora}>{data.data} - {data.hora}</Text>
       <View style={{ flex: 1,flexDirection: 'column',justifyContent: 'center',alignItems: 'center', }}>
      <Image style={{ width:340, height:200, marginBottom:10,}} source={{uri: data.fotoPrincipal.source}} />
      </View>
      <Text style={styles.creditofoto}>{data.creditoPrincipal}</Text>

      <Text style={styles.texto}>{data.texto}</Text>
  </View>
);

Noticia.propTypes = {
  data: propTypes.arrayOf(
    propTypes.shape({
      title: propTypes.string,
      legendaPrincipal: propTypes.string,
      data: propTypes.string,
      hora: propTypes.string,
      fotoPrincipal: propTypes.string,
      source: propTypes.string,
      creditoPrincipal: propTypes.string,
      texto: propTypes.string,
    })
  ).isRequired,
  navigation: propTypes.object.isRequired

};

export default Noticia;
