import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
 container:{
 	padding:10,
  width:"100%",
	},
 title:{
 	fontSize:20,
    fontWeight:'800',
  	color:'#000000',
    marginBottom:10,
 },
 legenda:{
    fontSize:15,
    fontWeight:'200',
    color:'#646464',
    marginBottom:10,
  },
  autor:{
	fontSize:14,
    fontWeight:'600',
    color:'#000000'
  },
 datehora:{
 	fontSize:12,
    fontWeight:'200',
    color:'#646464',
    marginBottom:10,
 },
 creditofoto:{
 	fontSize:10,
    fontWeight:'200',
    color:'#646464',
    marginBottom:10,
 },
 texto:{
 	lineHeight:30,
 	flex:1,
 	fontSize:18,
 	color:'#646464',
  marginBottom:50
 }
});

export default styles;