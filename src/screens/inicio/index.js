import React, { Component } from "react";
import styles from "./style";
import { View, Text, Image } from "react-native";
import { StackActions, NavigationActions } from "react-navigation";

export default class Inicio extends Component {
	componentDidMount() {
    	setTimeout(() => {
	      this.props.navigation.dispatch(
	        StackActions.reset({
	          index: 0,
	          actions: [NavigationActions.navigate({ routeName: "Login" })]
	        })
	      );
	    }, 4000);
	  }

	render() {
    	return (
	      <View style={styles.container}>
	        <Image source={{uri : 'https://media.giphy.com/media/ahza0v6s5pSxy/giphy.gif'}} style = {{width: 250, height: 400}} />
	      </View>
	    );
  }

}
