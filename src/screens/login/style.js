import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0090ff',
  },
  logo:{
    width:250,
    height:40,
    justifyContent: 'center',
    resizeMode:'contain',
    marginBottom:30
  },
  input:{
    width:"90%",
    height:50,
    backgroundColor:'rgba(225, 255, 255, 0.15)',
    borderRadius:5,
    color:'#FFFFFF',
    fontSize:17,
    marginBottom:10,
    padding:10
  },
  actionButton:{
    width:"90%",
    height:50,
    backgroundColor:'transparent',
    borderRadius:5,
    borderWidth:1,
    borderColor:'#FFFFFF',
    justifyContent:'center',
    alignItems:'center',
    margin:10
  },
  signButton:{
    width:"90%",
    height:50,
    backgroundColor:'transparent',
    justifyContent:'center',
    alignItems:'center',
    marginTop:40
  },
  actionButtonText:{
    color:'#FFFFFF',
    fontSize:17
  },
  signButtonText:{
    color:'#FFFFFF',
    fontSize:13
  }
});

export default styles;