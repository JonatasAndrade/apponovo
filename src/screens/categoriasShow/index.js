import React, { Component } from "react";
import { View, Text, Image, ScrollView, FlatList, Linking, ActivityIndicator } from "react-native";
import HeaderSider from "../../config/headersider";
import styles from "./style";
import NoticiaCate from "./components/noticia";
import { withNavigation } from "react-navigation";
import api from "../services/api";

class CategoriaShow extends Component {

  state = {
    data: [],
    loading: true
  };

  async componentDidMount() {
    await this.loadCategory();

    this.setState({ loading: false });
  }

  loadCategory = async () => {
    try {
      const categoria = this.props.navigation.state.params.category;
      const {
        data: { data }
      } = await api.get(`/noticias/list/detalhes?categoria=${categoria}&forpage=150`);
      this.setState({ data });
    } catch (err) {}
  };

  render() {
    const { loading } = this.state;

    if (loading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <View >

        <HeaderSider />
        <ScrollView pagingEnable={true}>

          <FlatList
            data={this.state.data}
            renderItem={({ item }) => <NoticiaCate data={item} />}
            keyExtractor={item => item.id}
          />
        </ScrollView>
      </View>
    );
  }
}

export default withNavigation(CategoriaShow);