import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
 container: {
    backgroundColor:'#d3d3d3',
    flex:1,
    alignItems:'center',
    flexDirection:'column',
  },
  botao:{
    width:'100%',
    backgroundColor:'#FFFFFF',
    padding:10,
    marginBottom:10,
  },
  tbotao:{
  	fontSize:18,
    fontWeight:'700',
  	color:'#0090ff',
    marginBottom:10,
  },
  sub:{
    color:'#000000',
    fontSize:14,
    fontWeight:'400',
    marginBottom:10,
  },
  legenda:{
    fontSize:15,
    fontWeight:'200',
    color:'#646464',
    marginBottom:10,
  },
  horacate:{
    fontSize:12,
    fontWeight:'200',
    color:'#646464'
  }
});

export default styles;