import React from "react";
import propTypes from "prop-types";
import { Image, TouchableOpacity, Text, View } from "react-native";
import styles from "./style";

const Noticia = ({ data, navigation }) => (

  <View style={styles.container} >
     
      <TouchableOpacity style={styles.botao}
      onPress={() => navigation.navigate("NoticiaLer", { category: data.id })}  underlayColor="#307EAF" >
      <Text style={styles.sub}>{data.categoria.title}</Text>
      <Text style={styles.tbotao}>{data.titleHome}</Text>
      <Text style={styles.legenda}>{data.legendaHome}</Text>
     
      <Text style={styles.horacate}>{data.hora} | {data.categoria.root.title}</Text>

      </TouchableOpacity>

  </View>
);

Noticia.propTypes = {
  data: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.string,
      categoria: propTypes.string,
      title: propTypes.string,
      titleHome: propTypes.string,
      legendaHome: propTypes.string,
      hora: propTypes.string,
      root: propTypes.string,
    })
  ).isRequired,
  navigation: propTypes.object.isRequired
};


export default Noticia;
