import React, { Component } from 'react';
import "../../config/reactotron";
import "../../config/sidebar";
import { Icon } from 'native-base';
import { Image } from "react-native";
import { createBottomTabNavigator, createAppContainer } from "react-navigation";

import Home from "../../screens/home";
import Noticias from "../../screens/noticias";
import Rádio from "../../screens/radio";
import Onibus from "../../screens/onibus";
import Guia from "../../screens/guia";

const Tabs = createBottomTabNavigator({
		Home: {
			screen: Home,
			navigationOptions : {
		tabBarIcon:(opt)=>{
			if (opt.focused) {
				return (
     				<Icon name='home' type={'FontAwesome'} style={{fontSize: 25, color: 'white'}} />
					);
			} else {
				return (
					 <Icon name='home' type={'FontAwesome'} style={{fontSize: 25, color: '#006498'}} />
					);
			}
		},
	},
		},
		 Noticias: {
			screen: Noticias,
			navigationOptions : {
		tabBarIcon:(opt)=>{
			if (opt.focused) {
				return (
     				<Icon name='newspaper-o' type={'FontAwesome'} style={{fontSize: 25, color: 'white'}} />
					);
			} else {
				return (
					 <Icon name='newspaper-o' type={'FontAwesome'} style={{fontSize: 25, color: '#006498'}} />
					);
			}
		}
	},
		},
		Rádio: {
			screen: Rádio,
			navigationOptions :{
		tabBarIcon:(opt)=>{
			if (opt.focused) {
				return (
     				<Icon name='radio' type={'Entypo'} style={{fontSize: 25, color: 'white'}}  />
					);
			} else {
				return (
					 <Icon name='radio' type={'Entypo'} style={{fontSize: 25, color: '#006498'}} />
					);
			}
		}
	},
		},
		Onibus: {
			screen: Onibus,
			navigationOptions : {
		tabBarIcon:(opt)=>{
			if (opt.focused) {
				return (
     				<Icon name='bus' type={'FontAwesome'} style={{fontSize: 25, color: 'white'}} />
					);
			} else {
				return (
					 <Icon name='bus' type={'FontAwesome'} style={{fontSize: 25, color: '#006498'}} />
					);
			}
		}
	},
		},
		Guia: {
			screen: Guia,
			navigationOptions : {
		tabBarIcon:(opt)=>{
			if (opt.focused) {
				return (
     				<Icon name='building' type={'FontAwesome'} style={{fontSize: 25, color: 'white'}} />
					);
			} else {
				return (
					 <Icon name='building' type={'FontAwesome'} style={{fontSize: 25, color: '#006498'}} />
					);
			}
		}
	}
		}

}, {
	defaultNavigationOptions: {
		    	headerLeft: null,
				  headerStyle: {
				    backgroundColor: "#0090ff",
				    height: 30,
				  },
				  headerTintColor: 'white',
		    },
	tabBarOptions:{
		showLabel:false,
		activeBackgroundColor:'#006498',
		inactiveBackgroundColor:'#0090ff',
      	showIcon: true

	}
});

export default createAppContainer(Tabs);