import React, { Component } from "react";
import { View, ScrollView, FlatList, Text, Image, ActivityIndicator } from "react-native";
import Header from "../../config/header";
import api from "../services/api";
import styles from "./style";
import NoticiaCat from "./components/noticiasItem";

export default class Noticias extends Component {
  state = {
    data: [],
    loading: true
  };

  componentDidMount() {
    this.loadCategories();
    this.setState({ loading: false });
  }

  loadCategories = async () => {
    try {
      const {
        data: { data }
      } = await api.get("/noticias/list?forpage=12");
      this.setState({ data });
    } catch (err) {}
  };

  render() {
    const { loading } = this.state;

    if (loading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
    
      <View >
        
         <Header />

        <ScrollView pagingEnable={true}>
          
          <FlatList
            data={this.state.data}
            renderItem={({ item }) => (
              <NoticiaCat navigation={this.props.navigation} data={item} />
            )}
            keyExtractor={item => item.id}
          />

        </ScrollView>
      </View>
    );
  }
}