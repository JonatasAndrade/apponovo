import React from "react";
import propTypes from "prop-types";
import { Image, TouchableOpacity, Text, View } from "react-native";
import styles from "./style";

const NoticiaItem = ({ data, navigation }) => (

  
  <View style={styles.container} >
     
      <TouchableOpacity style={styles.botao}
      onPress={() => navigation.navigate("NoticiaLer", { category: data.id })}  underlayColor="#307EAF" >
      <Text style={styles.sub}>{data.categoria.title}</Text>
      <Text style={styles.tbotao}>{data.titleHome}</Text>
      <Text style={styles.legenda}>{data.legendaHome}</Text>
     
      <View style={{ flex: 1,resizeMode:'contain',flexDirection: 'column',justifyContent: 'center',alignItems: 'center', }}>
      <Image style={{ width:500, height:200, marginBottom:10}} source={{uri: data.fotoPrincipal.source}} />
      </View>

      <Text style={styles.horacate}>{data.hora} | {data.categoria.root.title}</Text>

        </TouchableOpacity>

  </View>
);

NoticiaItem.propTypes = {
  data: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.string,
      categoria: propTypes.string,
      title: propTypes.string,
      titleHome: propTypes.string,
      legendaHome: propTypes.string,
      fotoPrincipal: propTypes.string,
      source: propTypes.string,
      hora: propTypes.string,
      root: propTypes.string,
    })
  ).isRequired,
  navigation: propTypes.object.isRequired
};

export default NoticiaItem;
